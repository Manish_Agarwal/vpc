resource "aws_vpc" "mainvpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_hostnames    = true

    tags = {
    Name                    = "vpc_tf"    
    }
  }
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow all out / allow SSH inbound traffic"
  vpc_id      = "${aws_vpc.mainvpc.id}"

  ingress {
    description = "SSH from outside"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security_group_tf"
  }
  depends_on = [aws_vpc.mainvpc]
}
resource "aws_internet_gateway" "GW_TF" {
  vpc_id = aws_vpc.mainvpc.id

  tags = {
    Name = "GW_TF"
  }
  depends_on = [aws_vpc.mainvpc]
}
