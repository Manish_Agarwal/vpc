resource "aws_route_table" "my_Route_table" {
  vpc_id = "${aws_vpc.mainvpc.id}"

  route {
    cidr_block = "0.0.0.0/24"
    gateway_id = "${aws_internet_gateway.GW_TF.id}"
        }
  

  tags = {
    Name = "my_Route_table"
  }
    depends_on = ["aws_vpc.mainvpc","aws_internet_gateway.GW_TF"]
}

resource "aws_route_table_association" "Publicroute_table_Association" {
  subnet_id      = aws_subnet.PublicSubnet_A.id
  route_table_id = aws_route_table.my_Route_table.id
  depends_on = ["aws_subnet.PublicSubnet_A","aws_route_table.my_Route_table"]
}