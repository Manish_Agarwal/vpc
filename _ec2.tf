resource "aws_instance" "public_EC2" {
  ami = "ami-047a51fa27710816e"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
  subnet_id =               "${aws_subnet.PublicSubnet_A.id}"
  tags = {
      Name = "Public_EC2"
  }
  depends_on = ["aws_vpc.mainvpc","aws_subnet.PublicSubnet_A"]
  key_name = "keypair_public"
  user_data = <<-EOF
                #!/bin/bash
                yum update -y
                yum install -y httpd
                systemctl start httpd.service
                systemctl enable httpd.service
                echo "Hi Friend , I am $(hostname -f) hosted by Terraform" > /var/www/html/index.html
                EOF
}
